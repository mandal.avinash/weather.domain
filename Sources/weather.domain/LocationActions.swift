//
//  LocationActions.swift
//  ReSwift
//
//  Created by Avinash Mandal on 6/2/19.
//

import ReSwift
import CoreLocation

public struct LocationActions {
    //Action Creator for triggering fetch location action
    static func actionGetLocation<T>(locationObserver: LocationObserver) -> Store<T>.ActionCreator {
        return { state, store in
            locationObserver.startObservingLocations()
            return WeatherDataAction.ShowLoading()
        }
    }
}

//Action containing location/error details obtained while fetching location which will upate the state used to trigger next flow
public struct ActionLocationData : Action {
    let locationDetails : CLLocation?
    let error: Error?
}

//TODO need to check working for Location Observer
//Class used for observibg location using CLLocationManager
class LocationObserver : NSObject {
    let locationManager: CLLocationManager
    let appStore : AppStore
    init(manager: CLLocationManager, store: AppStore) {
        locationManager = manager
        appStore = store
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        //TODO need to provide mechanism for asking permission
    }
    
    func startObservingLocations() {
        locationManager.startUpdatingLocation()
    }
}


//extension for LocationObserver class used to perform action fter fetching location/error details
extension LocationObserver : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
        appStore.dispatch(ActionLocationData(locationDetails: nil, error: error))
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            manager.stopUpdatingLocation()
            print("Dispatching fetchWeatherDataFromServer from LocationObserver")
            print("latitude : \(location.coordinate.latitude) , longitude: \(location.coordinate.longitude)")
            appStore.dispatch(ActionLocationData(locationDetails: location , error: nil))
        }
    }
}
