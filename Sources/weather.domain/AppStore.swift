//
//  AppStore.swift
//  ReSwift
//
//  Created by Avinash Mandal on 6/2/19.
//

import ReSwift
import CoreLocation

typealias AppStore = Store<WeatherState>

public let mainStore : Store = AppStore(
    reducer: WeatherReducer.weatherReducer as! AnyReducer,
    state: nil
)

public let weatherService : WeatherServiceDelegate = WeatherService(store: mainStore)
