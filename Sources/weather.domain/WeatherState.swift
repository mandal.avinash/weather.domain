//
//  WeatherState.swift
//  ReSwift
//
//  Created by Avinash Mandal on 6/2/19.
//

import ReSwift
import CoreLocation

public struct WeatherState: StateType {
    let isLoading: Bool
    let location : CLLocation?
    let weatherData : WeatherDataModel?
    let error : Error?
    let cityName : String?
}
