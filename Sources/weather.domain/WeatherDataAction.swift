//
//  WeatherDataAction.swift
//  ReSwift
//
//  Created by Avinash Mandal on 6/2/19.
//
import ReSwift
import CoreLocation

public struct WeatherDataAction {
    //Action Creator for triggering fetch weather for location action
    static func actionLocationWeatherData(state: WeatherState, store: AppStore) -> Action? {
        let location = state.location
        if let location = location {
            weatherService.getWeatherDataLocation(location: location)
        }
        
        return ShowLoading()
    }
    
    //Action Creator for triggering fetch weather for city action
    static func actionCityWeatherData(state: WeatherState, store: AppStore) -> Action? {
        let city = state.cityName
        if let city = city {
            weatherService.getWeatherDataCity(cityName: city)
        }
        return ShowLoading()
    }
    
    //Action to start fetching weather details
    struct ShowLoading : Action {}
    
    //Action to start fetching weather details for city using city name fetched
    public struct FetchCityWeatherData : Action {
        let cityName : String
    }
    
    //Action to display weather details fetched from server
    public struct ShowWeatherData: Action {
        let model : WeatherDataModel?
    }
    
    //Action to display error details fetched from server
    public struct ErrorData : Action {
        var error: Error?
    }
    
    public class DynamicError : Error {
        let errorString : String
        init(message: String) {
            errorString = message
        }
    }
}

public protocol WeatherServiceDelegate {
    func getWeatherDataCity(cityName : String)
    func getWeatherDataLocation(location: CLLocation)
}

class WeatherService: WeatherServiceDelegate {
    
    let WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather"
    let APP_ID = "9c2aa49ac3e08897a35cb13a996b282c"
    let store : AppStore
    
    init(store: AppStore) {
        self.store = store
    }
    
    func getWeatherDataCity(cityName: String) {
        let parameters = ["q" : cityName , "appid": APP_ID]
        getWeatherData(params: parameters)
    }
    
    func getWeatherDataLocation(location: CLLocation) {
        let parameters = ["lat" : String(location.coordinate.latitude), "lon": String(location.coordinate.longitude) , "appid": APP_ID]
        getWeatherData(params: parameters)
    }
    
    func getWeatherData(params : [String: String]) {
        
//        Alamofire.request(WEATHER_URL, method: .get, parameters: params).responseJSON{
//            response in
//            if(response.result.isSuccess){
//                print("Successful response")
//                if let weatherData = response.result.value {
//
//                    if let weatherModel = self.setWeatherModel(JSON(weatherData)) {
//                        self.store.dispatch(ShowWeatherData(model: weatherModel))
//                    }else{
//                        print("Weather data temperature is null")
//                        self.store.dispatch(ErrorData(error: DynamicError(message: "Weather data temperature is null")))
//                    }
//                }else{
//                    print("Weather data is null")
//                    self.store.dispatch(ErrorData(error: DynamicError(message: "Weather data is null")))
//                }
//            }else {
//                print("Error : \(response.result.error!)")
//                self.store.dispatch(ErrorData(error: response.result.error!))
//            }
//        }
    }
    
//    func setWeatherModel(_ json: JSON) -> WeatherDataModel? {
//        var weatherDataModel : WeatherDataModel? = nil
//        if let temperature = json["main"]["temp"].double{
//            weatherDataModel = WeatherDataModel()
//            weatherDataModel?.temperature = Int(temperature - 273.15)
//            weatherDataModel?.city = json["name"].stringValue
//            weatherDataModel?.condition = json["weather"][0]["id"].intValue
//            weatherDataModel?.weatherIcon = weatherDataModel!.updateWeatherIcon(condition: weatherDataModel!.condition)
//        }
//        return weatherDataModel
//    }
    
    
}
