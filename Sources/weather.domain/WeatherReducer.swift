//
//  WeatherReducer.swift
//  ReSwift
//
//  Created by Avinash Mandal on 6/2/19.
//

import ReSwift
import CoreLocation

//Reducer used to provide updated states according to the action recieved
public struct WeatherReducer {
    
    public static func weatherReducer(action: Action, state: WeatherState?)-> WeatherState {
        
        switch action {
        case is WeatherDataAction.ShowLoading:
            print("Fetch Location Action executed using Reducer")
            return WeatherState(isLoading: true, location:nil ,  weatherData : nil , error: nil, cityName : nil)
            
        case let action as ActionLocationData:
            print("Fetch Weather Location Data Action executed using Reducer")
            return WeatherState(isLoading: false, location: action.locationDetails ,  weatherData : nil , error: action.error, cityName : nil)
            
        case let action as WeatherDataAction.ShowWeatherData:
            print("Show Weather Data Action executed using Reducer")
            return WeatherState(isLoading: false, location: nil, weatherData: action.model, error: nil, cityName : nil)
            
        case let action as WeatherDataAction.ErrorData:
            print("Error data Action executed using Reducer")
            return WeatherState(isLoading: false, location: nil, weatherData: nil, error: action.error, cityName : nil)
            
        case let action as WeatherDataAction.FetchCityWeatherData:
            return WeatherState(isLoading: false, location: nil, weatherData: nil, error: nil, cityName : action.cityName)
            
        default:
            break
        }
        
        return state ?? WeatherState(isLoading: false, location: nil ,  weatherData: nil , error: nil , cityName : nil)
    }
    
}

