import XCTest

import weather_domainTests

var tests = [XCTestCaseEntry]()
tests += weather_domainTests.allTests()
XCTMain(tests)