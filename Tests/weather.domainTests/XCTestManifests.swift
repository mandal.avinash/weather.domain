import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(weather_domainTests.allTests),
    ]
}
#endif